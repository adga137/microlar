
# DOCUMENTATION FOR INSTALLATION

# Requirements

  php		>=	v5.5
  apache	>=	v2.2
  mariadb	>=	v10
  mysql		>=	v5.5
  
// web browser's
  
  firefox	>=	v40  
  chrome	>=	v50
  chromium	>=	v50
  IE		>=	V9
  opera		>=	v40

# NOTE: PHP must have shell_exec enabled

# DATA BASE CONFIG

  
# This proyect licence:

GPLv3 [https://www.gnu.org/licenses/gpl-3.0.html]

<?php

// establishing default
date_default_timezone_set('America/Caracas');

// view errors
error_reporting(E_ALL);
ini_set('display_errors', '1');

// sha256 key encode pass.
$key = "179ABC36C49C7AE37D6456A70BC46C7E2F6C675869D52E11420953649A06C141"; 

// Email de contact
$contactmail="admin@yourdomain.com";

function surl($url){
    return dirname(dirname(dirname(__file__))).DS.$url;
}

function url($url){
	$url_base = 'http://www.yourdomain.com/';

	if($_SERVER['HTTP_HOST'] == 'localhost'){
	  $split = explode('/',dirname(dirname(dirname(__FILE__))));
	  $url_base = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/'.$split[count($split)-1].'/';
	}

	return $url_base.$url;
}


include surl("app".DS."config".DS."db.cfg.php");

set_lang("es_ve");

function set_lang($setl){
	include surl("app".DS."lang".DS.$setl.DS."lang.php");
	$GLOBALS["lang"] = $lang;
	$GLOBALS["jslang"] = $jslang;
}
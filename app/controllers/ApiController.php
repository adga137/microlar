<?php

require_once surl("app".DS."controllers".DS."CountryController.php");
require_once surl("app".DS."controllers".DS."UserController.php");

class ApiController{

	public function __construct(){}

	public function run($params, $data=NULL, $files=NULL){
		
		switch ($params) {
			case 'usergetdata':
				$userdata = (new UserController)->getuserdata($data);
				unset($userdata["password"]);
				$userdata["country_name"] = (new CountryController)->getcountryname($userdata["country"]);
				$userdata["lvlaccess_name"] = (new UserController)->getnamelvlaccess($userdata["lvlaccess"]);
				echo json_encode($userdata);
				break;

			case 'useredit':
				$userdata = (new UserController)->getuserdata($data);
				unset($userdata["password"]);
				echo json_encode($userdata);
				break;

			case 'updatemypass':
				$userdata = (new SessionController)->getuserdata();

				if($userdata["password"]==sha1(crypt($data["currentpassword"],$GLOBALS["key"]))){
					if((new UserController)->updatemypass($data)){
						echo json_encode(array('status' => 0));
					}else{
						echo json_encode(array('status' => 2));
					}
				}else{
					echo json_encode(array('status' => 1));
				}
				break;

			case 'deleteuser':
				$res = (new UserController)->delete($data["id"]);
				echo json_encode(array('res' => $res ));
				break;

			case 'userstore':
				$res = (new UserController)->store($data);
				echo json_encode(array('res' => $res ));
				break;


			case 'updateprofile':
				
				$res = (new UserController)->updateprofile($data, $files);

				echo json_encode(array('status' => $res));
				
				break;


			default:
				echo json_encode(array("param" => false));
				break;
		}

	}




	//destructor de la clase
  	public function __destruct(){}

}
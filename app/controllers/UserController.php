<?php

require_once surl("app".DS."model".DS."User.php");
require_once surl("app".DS."model".DS."Country.php");
require_once surl("app".DS."controllers".DS."SessionController.php");

class UserController {

  public $lang;

 	public function __construct(){
    global $lang;
  }

  //abriendo el home del usuario
  public function home(){
    
      $pwd = $GLOBALS["pwd"];
      include surl("app".DS."view".DS."user".DS."admin_home.php");

  }

  // invalid route
  public function notfound(){
    $pwd = $GLOBALS["pwd"];
    include surl("app".DS."view".DS."user".DS."404.php");
  }

  // user list type
  public function listusers( $page=1, $search=null ){
    $pwd = $GLOBALS["pwd"];

    $userlistpages = (new User)->userlist($page, $search);
    $countries = (new Country)->countrieslist();
    $lvlaccesslist  = (new User)->lvlaccesslist();
    include surl("app".DS."view".DS."user".DS."admin_users.php");
  }

  public function create(){
    $pwd = $GLOBALS["pwd"];
    $countries = (new Country)->countrieslist();
    $lvlaccess = (new User)->lvlaccesslist();
    include surl("app".DS."view".DS."user".DS."admin_createuser.php");
    
  }

  public function profile(){
    $pwd = $GLOBALS["pwd"];
    $countries = (new Country)->countrieslist();
    $lvlaccesslist  = (new User)->lvlaccesslist();
    $datasesionuser = (new SessionController)->getuserdata();
    include surl("app".DS."view".DS."user".DS."profile.php");
  }

  public function editprofile(){
    $pwd = $GLOBALS["pwd"];
    $countries = (new Country)->countrieslist();
    $lvlaccesslist  = (new User)->lvlaccesslist();
    $datasesionuser = (new SessionController)->getuserdata();
    include surl("app".DS."view".DS."user".DS."editprofile.php");
  }

  public function sessionchangepass(){
    $pwd = $GLOBALS["pwd"];
    $datasesionuser = (new SessionController)->getuserdata();
    include surl("app".DS."view".DS."user".DS."user_changepass.php");
  }

  public function updatemypass($data){
    $datasesionuser = (new SessionController)->getuserdata();
    return (new User)->updatemypass($datasesionuser["id"],$data);
  }

  public function updateprofile($data, $files){
    
  }

  // get access name 
  public function getnamelvlaccess($lvlaccess){
    $user = new User();
    return $user->namelvlaccess($lvlaccess);
  }

  public function finduser($search,$status){
    $pwd = $GLOBALS["pwd"];
    $user = new User();
    $userlist = $user->userfind($search,$status);
    include surl("app".DS."view".DS."user".DS."admin_users.php");
  }

  // return user data format json
  public function userview($userid){
    $user = new User();
    $userdata = $user->getuserdata($userid);
    echo json_encode($userdata);
  }

  // return user data array php
  public function getuserdata($id){
    $user = new User();
    return $user->getuserdata($id);
  }

  // del user
  public function delete($id){
    $userdata = $this->getuserdata($id);
    /**
    * res = 1 -> not permited
    * res = 2 -> internal error
    **/
    $res = 0;
    
    if($userdata["lvlaccess"] == 1 ){
      $res = 1;
    }else{
      if(!(new User())->delete($id)){
        $res = 2;
      }
    }

    return $res;
  }


  public function userresetpassword($userid){
    $user = new User();
    echo json_encode([ "status" => $user->resetpassword($userid) ]);
  }

  //login user
  public function login($login){
    $user = new User();

    $userid = $user->checkuser($login);

    

    if($userid!=false){
      $session=new SessionController();
      if($session->createsession($userid)) $a=0;
        header("location: ".url("index.php?a=home"));
    }else{
      header("location: ".url("index.php?a=login&e=1"));
    }
  }

  // send recorver pass mail
  public function sendrecoverpass($email){
    $user = new User();
    $datauser = $user->getuserdatabyemail($email);
    if($datauser!=false){

        $rptoken = $user->rptoken($email);

        $to        = $datauser["email"];
        $subject   = 'Recuperación de Contraseña - Microlar PHP microframework';
        $message   =  "------------------------------\n";
        $message   .= " Microlar PHP microframework";
        $message   .= "------------------------------\n";
        $message   .= "\n";
        $message   .= "Usted esta recibiendo el enlace de recuperación de contraseña.\n";
        $message   .= "Si usted no ha realizado esta accion comuniquese con el administrador.\n";
        $message   .= "Su Enlace para recuperación de contraseña es el siguiente:\n\n\n";
        $message   .= url("index?a=changepass&rptoken=".$rptoken);

        $heads = 'From: airdance@live.com' . "\r\n" .
            'Reply-To: airdance@live.com' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        mail($to, $subject, $message, $heads);

        echo json_encode([ "status" => true ]);

    }else{
      echo json_encode([ "status" => false ]);
    }
  }

  // change pass form
  public function changepass($rptoken){

    $status = (new User)->verifyrptoken($rptoken);

    include surl("app".DS."view".DS."changepass.php");
  }

  // process change pass
  public function processchangepass($data){
    if((new User)->changepass($data)){
      echo json_encode([ "status" => true ]);
    }
    echo json_encode([ "status" => false ]);
  }

  // update data user
  public function updateuser($data){
    $user = new User();
    echo json_encode([ "status" => $user->update($data) ]);
  }

  // creating user data
  public function store($data){
    $user = new User();

    $res = $user->store($data);

    return $res;
  }

  public function manual(){
    //redirect to manual

  }

  //constructor de la clase
  public function __destruct(){}

}

?>

<?php

require_once surl("app".DS."model".DS."Country.php");

class CountryController{

	public function __construct(){}

	public function getcountryname($countrycode){
		return (new Country)->getcountryname($countrycode);
	}

	//destructor de la clase
  	public function __destruct(){}

}
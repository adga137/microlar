<?php

require_once surl("app".DS."model".DS."User.php");

class SessionController {

	public $lang;
	//contructor de la clase
	public function __contruct(){
		global $lang;
	}

	//creando la sesión de usuario
	public function createsession($iduser){
		//crear el token de sesión tanto en la cookie como en la base de datos
		$user = new User();
		$token = $user->createsessiontoken($iduser);
		if($token!=false){
			setcookie('token', $token[0]["token"], time() + 86400);
			return true;
		}else{
			return false;
		}

	}

	//destruir la sesión de usuario
	public function logout(){
		//Eliminar el token de sesión tanto en la cookie como en la base de datos
		$user = new User();
		if(isset($_COOKIE["token"])){
			$user->deltoken($_COOKIE["token"]);
			setcookie('token', null, time() - 1000);
			header("location: ".url("index.php"));
		}else{
			header("location: ".url("index.php"));
		}
	}

	public function checksession(){
		//verificación de sesión activa
		$user = new User();

		if(isset($_COOKIE['token']))
			if($user->checktoken($_COOKIE['token']))
				return true;

		return false;
	}

	public function userlvlaccess(){
		//verificación de sesión activa
		$user = new User();

		if(isset($_COOKIE['token']))
			if($user->checktoken($_COOKIE['token'])){
				return $user->getlvlaccess($_COOKIE['token']);
			}

		return false;
	}

	public function userfullname(){
		//verificación de sesión activa
		$user = new User();

		if(isset($_COOKIE['token']))
			if($user->checktoken($_COOKIE['token'])){
				return $user->getfullname($_COOKIE['token']);
			}

		return false;
	}



	public function getuserdata(){
		//verificación de sesión activa
		$user = new User();

		if(isset($_COOKIE['token']) and isset($_COOKIE['token']))
			if($user->checktoken($_COOKIE['token'])){
				return $user->sessionuserdata($_COOKIE['token']);
			}

		return false;
	}

	public function access($typeaccess){
		$datauser = $this->getuserdata();
		$useraccess = json_decode($datauser['access'],true);
		
		//accesos
		switch ($datauser["lvlaccess"]) {
			case 1: //Administrator
				return true;
				break;

			default:
				# code....
				break;
		}
	}


	// destructor de la clase
	public function __destruct(){}
}
<!DOCTYPE html>
<html>
<head>
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="stylesheet" type="text/css" href="<?php echo url("vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo url("vendor/sweetalert-master/dist/sweetalert.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo url("css/userstyle.css"); ?>">
	<title>Microlar Framework</title>
</head>
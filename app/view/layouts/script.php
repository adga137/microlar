<script type="text/javascript" src="<?php echo url("vendor/jquery-2.2.4/jquery-2.2.4.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo url("vendor/bootstrap-3.3.7-dist/js/bootstrap.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo url("js/user/admin_users.js"); ?>"></script>
<script type="text/javascript" src="<?php echo url("vendor/sweetalert-master/dist/sweetalert.min.js"); ?>"></script>
<script type="text/javascript">
	var lang = <?php echo json_encode($GLOBALS["jslang"]); ?>;
</script>
<?php // user level access script  ?>
<?php if( (new SessionController)->userlvlaccess()==1 and $pwd=="users" ){ ?>
<script type="text/javascript">
	var	urlbs = "<?php echo url(""); ?>";

	$("body").find("#find").on("keypress", function (e){
		window.searchuser();
	});
	$("body").find("#buttonfind").on("click", function (e){
		window.searchuser();
	});
	$(".panel-body").on("click","#delete", function (e){
		var id = $(this).attr("userid");
		window.deleteuser(id);
	});
	$(".panel-body").on("click", "#edit",function (e){
		var id = $(this).attr("userid");
		window.edituser(id);
	});
	$(".panel-body").on("click", "#view",function (e){
		var id = $(this).attr("userid");
		window.viewuser(id);
	});
	$(".panel-body").on("click","#adduser",function (e){
		window.location = "<?php echo url("index.php?a=createuser"); ?>";
	});
</script>
<?php } ?>



<?php if ((new SessionController)->userlvlaccess()==1 and $pwd=="createuser") { ?>

<script type="text/javascript" src="<?php echo url("js/user/adduser.js"); ?>"></script>

<script type="text/javascript">
	var	urlbs = "<?php echo url(""); ?>";


	$(".panel-body").on("submit","#formadduser",function (e){
		window.adduser();
	});

	function gotouserlist(){
		window.location = "<?php echo url("index.php?a=users"); ?>";
	}

</script>

<?php } ?>

<?php if( $pwd=="changemypass" ){ ?>
<script type="text/javascript" src="<?php echo url("js/user/changemypass.js"); ?>"></script>
<script type="text/javascript">
	var	urlbs = "<?php echo url(""); ?>";

	$("body").find("#formchangemypass").on("submit", function (e){
		window.changemypass();
	});

</script>
<?php } ?>


<?php if( $pwd=="editprofile" ){ ?>
<script type="text/javascript" src="<?php echo url("js/user/updateprofile.js"); ?>"></script>
<script type="text/javascript">
	var	urlbs = "<?php echo url(""); ?>";

	$("body").find("#formprofile").on("submit", function (e){
		e.preventDefault();
		window.updateprofile();
	});

	$("body").find("#userimg").on("change", function (e){
		

		$("body").find("#userimg").attr(
				"src",
				imageToBase64(
					document.querySelector('#vuserimg'),
					document.querySelector('#userimg').files[0],
					)
			);
	})
</script>
<?php } ?>

</html>


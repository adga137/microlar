<body>
	<nav class="navbar navbar-default">
		<div class="container-fluid">
			<div class="navbar-header">
				<button type="button" class="collapsed navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a href="<?php echo url("index.php?a=home"); ?>" class="navbar-brand">Microlar</a>
			</div>
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
				<ul class="nav navbar-nav navbar-right">
					<!--<li><a href="#">Link</a></li>-->
					<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img class="avatarimg" src="<?php echo url("img/avatar.png"); ?>"> <?php
						$userdata = (new SessionController)->getuserdata();
						echo $userdata["firstname"]." ".$userdata["lastname"];
						?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo url("index.php?a=profile"); ?>"><span class="glyphicon glyphicon-list-alt"></span> <?php echo $GLOBALS["lang"]["profile"]; ?></a></li>
							<li><a href="<?php echo url("index.php?a=changemypass"); ?>"><span class="glyphicon glyphicon-pencil"></span> <?php echo $GLOBALS["lang"]["change_pass"]; ?></a></li>
							<!--<li><a href="#">Something else here</a></li>-->
							<li role="separator" class="divider"></li>
							<li><a href="<?php echo url("index.php?a=logout"); ?>"><span class="glyphicon glyphicon-off"></span> <?php echo $GLOBALS["lang"]["exit"]; ?></a></li>
						</ul>
					</li>
				</ul>


				<ul class="nav navbar-nav">
					<!--<li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
					<li><a href="#">Link</a></li>-->
					
					<li class="dropdown"> <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><span class="glyphicon glyphicon-user"></span> <?php echo $GLOBALS["lang"]["users"]; ?> <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<li><a href="<?php echo url("index.php?a=users"); ?>"><span class="glyphicon glyphicon-th-list"></span> <?php echo $GLOBALS["lang"]["userlist"]; ?></a></li>
							<!--
							<li><a href="#">Another action</a></li>
							<li><a href="#">Something else here</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="#">Separated link</a></li>
							-->
							<li role="separator" class="divider"></li>
							<li><a href="<?php echo url("index.php?a=createuser"); ?>"><span class="glyphicon glyphicon-plus"></span> <?php echo $GLOBALS["lang"]["adduser"]; ?></a></li>
						</ul>
					</li>
				</ul>
				<!--<form class="navbar-form navbar-left">
					<div class="form-group">
						<input class="form-control" placeholder="Search">
					</div>
					<button type="submit" class="btn btn-default">Submit</button>
				</form>-->
				
			</div>
		</div>
	</nav>

<!DOCTYPE html>
<html>
<head>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
	<link rel="stylesheet" type="text/css" href="<?php echo url("vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo url("css/style.css"); ?>">
	<title><?php echo $GLOBALS["lang"]["login"]; ?> Microlar Framework</title>
</head>
<body class="bg-body">
	<div class="container-fluid">
		<div class="row">
			<div class="box-login">
				<div class="box-login-header">
					<img src="<?php echo url("img/microlar.png"); ?>" class="img-logo">
					<p class="login-box-text"><?php echo $GLOBALS["lang"]["login"]; ?></p>
				</div>
				<div class="box-body" >
					<?php if(isset($_GET["e"])) if($_GET["e"]==1){ ?>
				    <p class="login-box-msg-error" style="color: red;">Usuario/Email o contraseña incorrectos</p>
				    <?php } ?>
					<form class=""  name="loginform" id="loginform" action="<?php echo url("index.php?a=access"); ?>" method="post" >
						<div class="box-login-content">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
									<input type="text" class="form-control" id="user" name="user" placeholder="<?php echo $GLOBALS["lang"]["user_email"]; ?>" />
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
									<input type="password" class="form-control" id="password" name="password" placeholder="<?php echo $GLOBALS["lang"]["password"]; ?>" />
								</div>
								
							</div>
							<div class="form-group">
								<label for="recorder"><input type="checkbox" name="recorder" id="recorder"> <?php echo $GLOBALS["lang"]["recorder"]; ?></label>
								<button class="btn btn-primary pull-right"><?php echo $GLOBALS["lang"]["enter"]; ?></button>
							</div>
						</div>
					</form>
					<a class="link-recover" href="<?php echo url("index.php?a=passrecover"); ?>"><?php echo $GLOBALS["lang"]["passrecover"]; ?></a>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="<?php echo url("vendor/jquery-2.2.4/jquery-2.2.4.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo url("vendor/bootstrap-3.3.7-dist/js/bootstrap.min.js"); ?>"></script>
</html>
<!DOCTYPE html>
<html>
<head>
	<title>Microlar PHP framework v0.01</title>
	<link rel="stylesheet" type="text/css" href="">
</head>
<body>
	<h1>Microlar PHP framework v0.01</h1>
	<p>
	<b>English:</b> This is a framework based on laravel framework. It does not seek to be better than other frameworks, it only seeks to help novice students and programmers understand in an easy way Object Oriented Programming in PHP. In the beginning it is designed to be simple, the development process is not too automated for people who have to develop the functionalities respecting the logic of Object Oriented Programming but programming as it should from scratch, but having an organizational structure similar to other frames<br/><br/>

	<b>Spanish:</b> Este es un framework basado en laravel framework. 
	No busca ser mejor que otros framework solo busca ayudar a estudiantes y programadores novatos a comprender de una manera sencilla la Programación Orientada a Objetos en PHP. 
	En un principio esta diseñado para ser sencillo, no automatiza demasiado el proceso de desarrollo para que las personas tengan que desarrollar las funcionalidades respetando la lógica de la Programación Orientada a Objetos pero programando como se debe desde cero, pero teniendo una estructura organizativa parecida a otros frameworks.<br/>
	</p>

	<h2>Charactheristics | Caracteristicas :</h2>

	<p><b>English:</b></p>
	<ul>
		<li>Configuration Directory in app / config that contains the basic configurations</li>

		<li>It is not necessary to use apache2 or lighttp mod_rewrite routing is basic by GET method and a control of choice (SWITCH)</li>
		
		<li>Pre-built user management with SessionController and UserController controllers</li>
		
		<li>Preconstructed ApiController controller</li>
	</ul>

	<p><b>Spanish:</b></p>
	<ul>
		<li>Directorio de Configuracion en app/config que contiene las configuraciones basicas</li>
		<li>No es necesario usar mod_rewrite de apache2 o lighttp el enrutamiento se básico por metodo GET y un control de desición (SWITCH)</li>
		<li>Preconstruida la gestión de los usuarios con los controladores SessionController y UserController</li>
		<li>Controlador ApiController preconstruido</li>
	</ul>


	<h2>Requeriments | Requisitos:</h2>

	<ul>
		<li>Apache2 >= v2 | lighttp >= v1.4</li>
		<li>Mysql >= v5.5 | MariaDB >= v10</li>

	</ul>	

	<h2>Note | Nota:</h2>

	<p>
		<b>English:</b>It is not necessary to compose, docker, or any other, if you want to extend its functionality you must download each of the libs you need and include them in the controller that needs this extra function.<br/>
		It was built to help developers who cannot be online 24 hours a day for different reasons (Venezuela case that the ISP's service is bad).
	</p>

	<p>
		<b>Spanish:</b> No es necesario composer, docker, o cualquier otro, si se quiere extender su funcionalidad se deberá descargar cada una de las libs que necesite e incluirlas en el controlador que necesite dicha función extra.<br/>
		El mismo fue construido para ayudar a desarrolladores que no pueden estar en línea las 24h por diferentes motivos (Caso venezuela que el servicio de los ISP's es pesimo).
	</p>

	<h2>Install | Instalación:</h2>

	<p>
		<b>English</b> If you download the project in zip, you only have to unzip it and copy the resulting directory to the Apache/lighttp server's working folder, which by default in GNU / Linux or UNIX systems is usually:<br/>
		
		<b>Spanish:</b> Si descarga el proyecto en zip, solo deberá descomprimir el mismo y copiar el directorio resultante en la carpeta de trabajo del servidor Apache/lighttp que por defecto en GNU/Linux o sistemas tipo UNIX suele ser:
	</p>
	<pre>/var/www/html</pre>
	<p>
		<b>English</b> Then you just have to import the database with the following steps:<br/>
		<b>Spanish:</b> Luego solo deberá importar la base de datos con los siguientes pasos:<br/>
	</p>
	<pre>mysql -u root -p</pre>
	<pre>CREATE DATABASE microlar;</pre>
	<pre>mysql -u admin -p microlar < database.sql</pre>
	
	<h2>User Access | Acceso de usuarios:</h2>

	<p>
		<b>English</b> microlar brings pre-built a user login and a basic user administration which you can access through the following url: [<a href="<?php echo url("index.php?a=login"); ?>">Login</a>]<br/>
		<b>Spanish:</b> microlar trae precontruida un inicio de sesión de usuarios y una administración de usuarios básica la cual puedes acceder mediante la siguiente url: [<a href="<?php echo url("index.php?a=login"); ?>">Login</a>]<br/>
		<pre>User|Usuario: admin</pre>
		<pre>Password|Contraseña: microlar</pre>
	</p>

	<h2>Donate | Donativos:</h2>

	<p>
	<b>English</b> Financing through donations would be helpful, since it would allow me to spend time in the Microlar framework and improve it, since without resources I am forced to work on other projects to be able to subsist your donation, it would be a great help to improve the Microlar framework.<br/>
	<b>Spanish:</b> Me sería de gran ayuda un financiamiento a través de donativos ya que me permitiría poder dedicarle tiempo a Microlar framework y mejorarlo ya que sin recursos me veo forzado a trabajar en otros proyectos  para poder subsistir tu donativo sería de gran ayuda para poder mejorar Microlar framework.<br/>
	</p>

	<form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
	<input type="hidden" name="cmd" value="_s-xclick">
	<input type="hidden" name="hosted_button_id" value="8EFR4YK6W39AU">
	<input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" border="0" name="submit" alt="PayPal. La forma rápida y segura de pagar en Internet.">
	<img alt="" border="0" src="https://www.paypalobjects.com/es_XC/i/scr/pixel.gif" width="1" height="1">
	</form>

	<p>
	<b>English</b> You can also make a donation to my bitcoin address: 1GoxoTSkTHiVoBGtcoxW72tHc4qrVb9NEG <br/>
	<b>Spanish</b> También puedes hacer un donativo a mi dirección de bitcoins: 1GoxoTSkTHiVoBGtcoxW72tHc4qrVb9NEG
	</p>

	<p>
	<b>English</b> Microlar will be updated as soon as possible with new functions requiring the minimum possible to operate<br/>
	<b>Spanish:</b> Microlar será actualizado a la brevedad posible con nuevas funciones requiriendo lo minimo posible para funcionar<br/>
	</p>
	<br/><br/>
</body>
</html>
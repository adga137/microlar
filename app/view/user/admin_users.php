<?php 

include surl("app".DS."view".DS."layouts".DS."htmlheader.php");

include surl("app".DS."view".DS."layouts".DS."header.php");
?>



<div>
		<nav class="pd-lr">
			<h1 class="texthap"><i class="glyphicon glyphicon-dashboard"></i> <?php echo $GLOBALS["lang"]["administration"]; ?> > <small><i class="glyphicon glyphicon-user"></i> <?php echo $GLOBALS["lang"]["users"]; ?> </small></h1>
		</nav>
</div>

<div class="panel panel-default m-l m-r">
  <!-- Default panel contents -->
  <div class="panel-heading"><span class="glyphicon glyphicon-th-list"></span> <?php echo $GLOBALS["lang"]["userlist"]; ?></div>

  <div class="panel-body">

  	<form id="formsearch" name="formsearch" action="" method="post" onsubmit="return false;">
	  	<div class="row">
	  		<div class="col-md-12">
			  	<div class="form-group">
            <button id="adduser" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> <?php echo $GLOBALS["lang"]["adduser"]; ?></button>
          </div>
          <div class="form-group">
			    	<div class="input-group">
						<input type="text" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["search_eun"]; ?>" aria-label="Busqueda" name="find" id="find">
						<div class="input-group-btn">
							<button type="button" class="btn btn-default" name="buttonfind" id="buttonfind"><?php echo $GLOBALS["lang"]["search"]; ?></button>
						</div>
			    	</div>
			    </div>
		    </div>
		</div>
	</form>
  	<table class="table">
  		<thead>
  			<tr>
  				<th>#</th>
  				<th><?php echo $GLOBALS["lang"]["user"]; ?></th>
  				<th class="resh9"><?php echo $GLOBALS["lang"]["email"]; ?></th>
  				<th class="resh6"><?php echo $GLOBALS["lang"]["access"]; ?></th>
  				<th><?php echo $GLOBALS["lang"]["status"]; ?></th>
  				<th></th>
  			</tr>
  		</thead>
  		<tbody>
  			<?php
  				$i=1;
  				foreach ($userlistpages["users"] as $user) {
  			?>
  			<tr class="lnh-30">
  				<th scope="row"><?php echo $i++; ?></th>
  				<td><?php echo $user["user"]; ?></td>
				<td class="resh9"><?php echo $user["email"]; ?></td>
  				<td class="resh6"><?php echo $GLOBALS["lang"]["lvlaccessname"][$user["lvlaccess"]]; ?></td>
  				<td><?php echo ($user["status"]==1)? "Desactivado" : "Activo" ; ?></td>
  				<td>
  					<button id="view" userid="<?php echo $user["id"]; ?>" class="btn btn-default btn-xs" data-toggle="modal" data-target="#viewModal"><span class="glyphicon glyphicon-eye-open" ></span></button>
  					<button id="edit" userid="<?php echo $user["id"]; ?>" class="btn btn-default btn-xs"data-toggle="modal" data-target="#editModal"><span class="glyphicon glyphicon-pencil" ></span></button>
  					<?php if($user["lvlaccess"]>1){ ?>
            <button id="delete" userid="<?php echo $user["id"]; ?>" class="btn btn-default btn-xs"><span class="glyphicon glyphicon-remove" ></span></button>
            <?php } ?>
  				</td>
  			</tr>
  			<?php } ?>
		</tbody>
	</table>
	<?php if( count($userlistpages["users"]) == 25 ){ ?>

    
    <nav class="fright" aria-label="User list pagination" >
      
      <ul class="pagination">


        <?php $actualpage=$userlistpages["actualpage"];  ?>
        
        <li class="page-item <?php if($actualpage==1) echo "disabled"; ?>"><a class="page-link" href="<?php if($actualpage>1) echo url("index.php?a=listusers&page=".($actualpage-1)); ?>"><?php echo $GLOBALS["lang"]["back"]; ?></a></li>


        <li class="page-item active"><a class="page-link" href=""><?php echo $actualpage; ?></a></li>


        <li class="page-item <?php if($actualpage==$userlistpages["totalpages"]) echo "disabled"; ?>"><a class="page-link" href="<?php if($actualpage<$userlistpages["totalpages"]) echo url("index.php?a=listusers&page=".($actualpage+1)); ?>"><?php echo $GLOBALS["lang"]["next"]; ?></a></li>

      </ul>
      <span style="display:block; text-align: right;">Total de paginas: <?php echo $userlistpages["totalpages"]; ?></span>

    </nav>            

    <?php }else{ ?>
    <nav class="fright" aria-label="User list pagination" >
      <span style="display:block; text-align: right; margin: 17px;">Total de paginas: <?php echo $userlistpages["totalpages"]; ?></span>
    </nav>
    <?php } ?>
  </div>
</div>



  <!-- Modal -->
<div class="modal fade" id="viewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left" id="exampleModalLabel"><span class="glyphicon glyphicon-user"></span> <?php echo $GLOBALS["lang"]["userdata"]; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["user"]; ?>:</label>
            <span id="user"></span>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["email"]; ?>:</label>
            <span id="email"></span>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["firstname"]; ?>:</label>
            <span id="firstname"></span>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["lastname"]; ?>:</label>
            <span id="lastname"></span>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["country"]; ?>:</label>
            <span id="country"></span>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["phone"]; ?>:</label>
            <span id="phone"></span>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["state_province"]; ?>:</label>
            <span id="state"></span>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["city"]; ?>:</label>
            <span id="city"></span>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["address"]; ?>:</label>
            <span id="address"></span>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["status"]; ?>:</label>
            <span id="status"></span>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["access"]; ?>:</span>
            <span id="access"></span>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <!--<button type="submit" class="btn btn-primary"></button>-->
      </div>
    </div>
  </div>
</div>
<!-- end modal -->

  <!-- Modal -->
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title pull-left" id="exampleModalLabel"><span class="glyphicon glyphicon-user"></span> <?php echo $GLOBALS["lang"]["updateuserdata"]; ?></h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form role="form" name="formedituser" id="formedituser" onsubmit="return false;" method="post" action="">
      <div class="modal-body">
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["user"]; ?>:</label>
            <span id="user"></span>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["password"]; ?>:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
              <input type="password" name="password" id="password" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_password"]; ?>" />
            </div>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["repassword"]; ?>:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
              <input type="password" name="repassword" id="repassword" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["repassword"]; ?>" />
            </div>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["email"]; ?>:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-envelope"></i></span>
              <input type="email" name="email" id="email" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_email"]; ?> " />
            </div>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["firstname"]; ?>:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
              <input type="text" name="firstname" id="forstname" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_firstname"]; ?>" />
            </div>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["lastname"]; ?>:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
              <input type="text" name="lastname" id="lastname" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_lastname"] ?>" />
            </div>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["country"]; ?>:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
              <select class="form-control" id="country" name="country">
              <option value=""><?php echo $GLOBALS["lang"]["select_country"]; ?></option>
              <option value=""></option>
              <?php foreach ($countries as $country) { ?>
              <option value="<?php echo $country["country_code"]; ?>"><?php echo $country["country_name"]; ?></option>
              <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["phone"]; ?>:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-earphone"></i></span>
              <input type="text" name="phone" id="phone" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_phone"] ?>" />
            </div>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["state_province"]; ?>:</label>
            <div class="input-group">
                <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
                <input type="text" name="state" id="state" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_stateprovince"]; ?>" />
            </div>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["city"]; ?>:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
          <input type="text" name="city" id="city" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_city"]; ?>" />
            </div>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["address"]; ?>:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
              <textarea class="form-control" name="address" id="address" placeholder="<?php echo $GLOBALS["lang"]["type_address"]; ?>"></textarea>
            </div>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["status"]; ?>:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-filter"></i></span>
              <select class="form-control" id="status" name="status">
              <option value="0"><?php echo $GLOBALS["lang"]["active"]; ?></option>
              <option value="1"><?php echo $GLOBALS["lang"]["disable"]; ?></option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label><?php echo $GLOBALS["lang"]["access"]; ?>:</label>
            <div class="input-group">
              <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-filter"></i></span>
              <select class="form-control" id="lvlaccess" name="lvlaccess">
              <?php $i=1; foreach ($GLOBALS["lang"]["lvlaccessname"] as $a) { ?>
              <option value="<?php echo $i++; ?>"><?php echo $a; ?></option>
              <?php } ?>
              </select>
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal"><?php echo $GLOBALS["lang"]["close"] ?></button>
        <button type="submit" class="btn btn-primary"><?php echo $GLOBALS["lang"]["update"]; ?></button>
      </div>
      </form>
    </div>
  </div>
</div>
<!-- end modal-->



<?php 
include surl("app".DS."view".DS."layouts".DS."footer.php");

include surl("app".DS."view".DS."layouts".DS."script.php");
?>

<?php 

include surl("app".DS."view".DS."layouts".DS."htmlheader.php");

include surl("app".DS."view".DS."layouts".DS."header.php");
?>


<div>
		<nav class="pd-lr">
			<h1 class="texthap"><i class="glyphicon glyphicon-dashboard"></i> <?php echo $GLOBALS["lang"]["administration"]; ?> > <small><i class="glyphicon glyphicon-user"></i> <?php echo $GLOBALS["lang"]["adduser"] ?></small></h1>
		</nav>
</div>

<div class="panel panel-default m-l m-r">
  <!-- Default panel contents -->
  <div class="panel-heading"><span class="glyphicon glyphicon-plus"></span> <?php echo $GLOBALS["lang"]["adduser"]; ?></div>

  <div class="panel-body">
  	<form name="formadduser" id="formadduser" action="" method="post" onsubmit="return false;">
  		<div class="form-group">
  			<label><?php echo $GLOBALS["lang"]["user"]; ?></label>
	  		<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
	  			<input type="text" name="user" id="user" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_user"]; ?>" />
	  		</div>
  		</div>
  		<div class="row">
  			<div class="col-sm-6">
				<div class="form-group">
					<label><?php echo $GLOBALS["lang"]["password"]; ?></label>
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
						<input type="password" name="password" id="password" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_password"]; ?>" />
					</div>
				</div>
			</div>
			<div class="col-sm-6">

				<div class="form-group">
					<label><?php echo $GLOBALS["lang"]["repassword"]; ?>:</label>
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
						<input type="password" name="repassword" id="repassword" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["repassword"]; ?>" />
					</div>
				</div>
			</div>
  		</div>
  		<div class="form-group">
  			<label><?php echo $GLOBALS["lang"]["email"]; ?>:</label>
  			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-envelope"></i></span>
  				<input type="email" name="email" id="email" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_email"]; ?>	" />
  			</div>
  		</div>
  		<div class="row">
  			<div class="col-sm-6">
		  		<div class="form-group">
		  			<label><?php echo $GLOBALS["lang"]["firstname"]; ?>:</label>
		  			<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
		  				<input type="text" name="firstname" id="forstname" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_firstname"]; ?>" />
		  			</div>
		  		</div>
		  	</div>
		  	<div class="col-sm-6">
		  		<div class="form-group">
		  			<label><?php echo $GLOBALS["lang"]["lastname"]; ?>:</label>
		  			<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
		  				<input type="text" name="lastname" id="lastname" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_lastname"] ?>" />
		  			</div>
		  		</div>
		  	</div>
		</div>
		<div class="form-group">
			<label><?php echo $GLOBALS["lang"]["country"]; ?>:</label>
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
				<select class="form-control" id="country" name="country">
	        	<option value=""><?php echo $GLOBALS["lang"]["select_country"]; ?></option>
	        	<option value=""></option>
	            <?php foreach ($countries as $country) { ?>
	            <option value="<?php echo $country["country_code"]; ?>"><?php echo $country["country_name"]; ?></option>
	            <?php } ?>
	          	</select>
          	</div>
		</div>
		<div class="form-group">
  			<label><?php echo $GLOBALS["lang"]["phone"]; ?>:</label>
  			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-earphone"></i></span>
  				<input type="text" name="phone" id="phone" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_phone"] ?>" />
  			</div>
  		</div>
  		<div class="form-group">
  			<label><?php echo $GLOBALS["lang"]["state_province"]; ?>:</label>
  			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
  				<input type="text" name="state" id="state" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_stateprovince"]; ?>" />
  			</div>
  		</div>
  		<div class="form-group">
  			<label><?php echo $GLOBALS["lang"]["city"]; ?>:</label>
  			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
  				<input type="text" name="city" id="city" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_city"]; ?>" />
  			</div>
  		</div>
  		<div class="form-group">
  			<label><?php echo $GLOBALS["lang"]["address"]; ?>:</label>
  			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
  				<textarea class="form-control" name="address" id="address" placeholder="<?php echo $GLOBALS["lang"]["type_address"]; ?>"></textarea>
  			</div>
  		</div>
  		<div class="form-group">
			<label><?php echo $GLOBALS["lang"]["status"]; ?>:</label>
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-filter"></i></span>
				<select class="form-control" id="status" name="status">
	            <option value="0"><?php echo $GLOBALS["lang"]["active"]; ?></option>
	            <option value="1"><?php echo $GLOBALS["lang"]["disable"]; ?></option>
	          	</select>
	        </div>
		</div>
  		<div class="form-group">
			<label><?php echo $GLOBALS["lang"]["lvlaccess"]; ?>:</label>
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-filter"></i></span>
				<select class="form-control" id="lvlaccess" name="lvlaccess">
	            <?php foreach ($lvlaccess as $a) { ?>
	            <option value="<?php echo $a["id"]; ?>"><?php echo $a["name"]; ?></option>
	            <?php } ?>
	          	</select>
	        </div>
		</div>
  		<div class="form-group">
  			<button type="reset" class="btn btn-warning"><?php echo $GLOBALS["lang"]["reset"]; ?></button>
  			<button type="submit" id="store" name="store" class="btn btn-primary pull-right"><?php echo $GLOBALS["lang"]["adduser"]; ?></button>
  		</div>
  	</form>
  </div>
</div>


<?php 
include surl("app".DS."view".DS."layouts".DS."footer.php");

include surl("app".DS."view".DS."layouts".DS."script.php");
?>
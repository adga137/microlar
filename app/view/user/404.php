<!DOCTYPE html>
<html>
<head>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
  <link rel="stylesheet" type="text/css" href="<?php echo url("vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css"); ?>">
  <link rel="stylesheet" type="text/css" href="<?php echo url("css/style.css"); ?>">
  <title><?php echo $GLOBALS["lang"]["login"]; ?> Micron Framework</title>
</head>
<body class="bg-body">
  <div class="container-fluid">
    <div class="row">
      
      <div class="box-adenied">
        <div class="box-adenied-header">
          <img src="<?php echo url("img/microlar.png"); ?>" class="img-logo">
          <p class="box-adenied-text"><?php echo $GLOBALS["lang"]["access"]; ?></p>
        </div>
        <!-- /.login-logo -->
        <div class="box-body">
          <p class="box-adenied-msg"><?php echo $GLOBALS["lang"]["accessdenied"]; ?></p>

          <a class="pull-left" href="<?php echo url("index.php?a=access"); ?>"><?php echo $GLOBALS["lang"]["back"]; ?></a><br>

        </div>
        <!-- /.login-box-body -->
      </div>
      <!-- /.login-box -->
    </div>
  </div>
</body>
<script type="text/javascript" src="<?php echo url("vendor/jquery-2.2.4/jquery-2.2.4.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo url("vendor/bootstrap-3.3.7-dist/js/bootstrap.min.js"); ?>"></script>
</html>
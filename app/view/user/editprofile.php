<?php 

include surl("app".DS."view".DS."layouts".DS."htmlheader.php");

include surl("app".DS."view".DS."layouts".DS."header.php");
?>


<div>
		<nav class="pd-lr">
			<h1 class="texthap"><i class="glyphicon glyphicon-dashboard"></i> <?php echo $GLOBALS["lang"]["administration"]; ?> > <small><i class="glyphicon glyphicon-user"></i> <?php echo $GLOBALS["lang"]["profile"] ?></small></h1>
		</nav>
</div>

<div class="panel panel-default m-l m-r">
  <!-- Default panel contents -->
  <div class="panel-heading"><span class="glyphicon glyphicon-user"></span> <?php echo $GLOBALS["lang"]["profile"]; ?></div>

  <div class="panel-body">
  	<form name="formprofile" id="formprofile" action="" method="post" onsubmit="return false;">
	  <div class="form-group dicontent">
	  	<img class="imguserprofile" name="vuserimg" id="vuserimg" width="150px" height="150px" src="<?php echo url("img/avatar.png"); ?>">
	  	<div>
	  		<input type="file" name="userimg" id="userimg" class="imgfileinput" />
	  		<label for="userimg" class="btn btn-default"><?php echo $GLOBALS["lang"]["change"]; ?></label>
	  	</div>
	  </div>
	  <div class="form-group">
	  	<label><?php echo $GLOBALS["lang"]["user"]; ?>:</label>
	  	<div class="input-group">
			<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
			<input type="text" class="form-control" name="user" id="user" value="<?php echo $userdata["user"]; ?>" placeholder="<?php echo $GLOBALS["lang"]["type_user"]; ?>"/>
		</div>
	  </div>

	  <div class="form-group">
	  	<label><?php echo $GLOBALS["lang"]["email"]; ?>:</label>
	  	<div class="input-group">
			<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-envelope"></i></span>
	  		<input type="text" class="form-control" name="email" id="email" value="<?php echo $userdata["email"]; ?>" placeholder="<?php echo $GLOBALS["lang"]["type_email"]; ?>" />
	  	</div>
	  </div>
	  <div class="form-group">
		  <div class="row">
		  	<div class="col-sm-6">
		  		<label><?php echo $GLOBALS["lang"]["firstname"]; ?>:</label>
		  		<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
		  			<input type="text" name="firstname" id="firstname" class="form-control" value="<?php echo $userdata["firstname"]; ?>" placeholder="<?php echo $GLOBALS["lang"]["type_firstname"]; ?>"; />
		  		</div>
		  	</div>
		  	<div class="col-sm-6">
		  		<label><?php echo $GLOBALS["lang"]["lastname"]; ?>:</label>
		  		<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
		  			<input type="text" name="lastname" id="lastname" class="form-control" value="<?php echo $userdata["lastname"]; ?>" placeholder="<?php echo $GLOBALS["lang"]["type_lastname"]; ?>"; />
		  		</div>
		  	</div>
		  </div>
	  </div>
	  <div class="form-group">
		  <div class="row">
		  	<div class="col-sm-6">
				<label><?php echo $GLOBALS["lang"]["country"]; ?>:</label>
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
					<select class="form-control" id="country" name="country">
		        	<option value=""><?php echo $GLOBALS["lang"]["select_country"]; ?></option>
		        	<option value=""></option>
		            <?php foreach ($countries as $country) { ?>
		            <option value="<?php echo $country["country_code"];?>" <?php echo ($country["country_code"]==$userdata["country"])? "selected" : "" ; ?> ><?php echo $country["country_name"]; ?></option>
		            <?php } ?>
		          	</select>
		      	</div>
	      	</div>
	      	<div class="col-sm-6">
				<label><?php echo $GLOBALS["lang"]["phone"]; ?>:</label>
				<div class="input-group">
					<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-earphone"></i></span>
					<input type="text" name="phone" id="phone" class="form-control" value="<?php echo $userdata["phone"]; ?>" placeholder="<?php echo $GLOBALS["lang"]["type_phone"] ?>" />
				</div>
			</div>
		  </div>
	  </div>
	  <div class="form-group">
  			<label><?php echo $GLOBALS["lang"]["state_province"]; ?>:</label>
  			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
  				<input type="text" name="state" id="state" class="form-control" value="<?php echo $userdata["state"]; ?>" placeholder="<?php echo $GLOBALS["lang"]["type_stateprovince"]; ?>" />
  			</div>
  		</div>
  		<div class="form-group">
  			<label><?php echo $GLOBALS["lang"]["city"]; ?>:</label>
  			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-map-marker"></i></span>
  				<input type="text" name="city" id="city" class="form-control" value="<?php echo $userdata["city"]; ?>" placeholder="<?php echo $GLOBALS["lang"]["type_city"]; ?>" />
  			</div>
  		</div>

  		<div class="form-group">
			<div class="row">
	  			<div class="col-sm-6">

					<label><?php echo $GLOBALS["lang"]["status"]; ?>:</label>
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-filter"></i></span>
						<select class="form-control" id="status" name="status">
			            <option value="0" <?php echo ($userdata["status"]==0)? "selected" : "" ; ?>><?php echo $GLOBALS["lang"]["active"]; ?></option>
			            <option value="1"  <?php echo ($userdata["status"]==1)? "selected" : "" ; ?>><?php echo $GLOBALS["lang"]["disable"]; ?></option>
			          	</select>
			        </div>
			    </div>
			    <div class="col-sm-6">
					<label><?php echo $GLOBALS["lang"]["lvlaccess"]; ?>:</label>
					<div class="input-group">
						<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-filter"></i></span>
						<select class="form-control" id="lvlaccess" name="lvlaccess">
			            <?php foreach ($lvlaccesslist as $a) { ?>

			            <option value="<?php echo $a["id"]; ?>" <?php echo ($a["id"]==$userdata["lvlaccess"])? "select" : "" ; ?>><?php echo $a["name"]; ?></option>
			            
			            <?php } ?>
			          	</select>
			        </div>
			    </div>
			</div>
		</div>
	  <div class="form-group">
	  	<button type="reset" class="btn btn-warning">Limpiar</button>
	  	<button type="submit" class="btn btn-primary pull-right"><?php echo $GLOBALS["lang"]["update"]; ?> <?php echo $GLOBALS["lang"]["profile"]; ?></button>
	  </div>
	</form>
  </div>
</div>





<?php 
include surl("app".DS."view".DS."layouts".DS."footer.php");

include surl("app".DS."view".DS."layouts".DS."script.php");
?>
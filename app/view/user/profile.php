<?php 

include surl("app".DS."view".DS."layouts".DS."htmlheader.php");

include surl("app".DS."view".DS."layouts".DS."header.php");
?>


<div>
		<nav class="pd-lr">
			<h1 class="texthap"><i class="glyphicon glyphicon-dashboard"></i> <?php echo $GLOBALS["lang"]["administration"]; ?> > <small><i class="glyphicon glyphicon-user"></i> <?php echo $GLOBALS["lang"]["profile"] ?></small></h1>
		</nav>
</div>

<div class="panel panel-default m-l m-r">
  <!-- Default panel contents -->
  <div class="panel-heading"><span class="glyphicon glyphicon-user"></span> <?php echo $GLOBALS["lang"]["profile"]; ?></div>

  <div class="panel-body">
	  <div class="dicontent">
	  	<img class="imguserprofile" src="<?php echo url("img/avatar.png"); ?>">
	  </div>
	  <div class="row">
	  	<div class="col-md-12">
	  		<strong><?php echo $GLOBALS["lang"]["user"]; ?>:</strong>
	  		<p><?php echo $userdata["user"]; ?></p>
	  	</div>
	  </div>

	  <div class="row">
	  	<div class="col-md-12">
	  		<strong><?php echo $GLOBALS["lang"]["email"]; ?>:</strong>
	  		<p><?php echo $userdata["email"]; ?></p>
	  	</div>
	  </div>
	  <div class="row">
	  	<div class="col-xs-6">
	  		<strong><?php echo $GLOBALS["lang"]["firstname"]; ?>:</strong>
	  		<p><?php echo $userdata["firstname"]; ?></p>
	  	</div>
	  	<div class="col-xs-6 tcenter">
	  		<strong><?php echo $GLOBALS["lang"]["lastname"]; ?>:</strong>
	  		<p><?php echo $userdata["lastname"]; ?></p>
	  	</div>
	  </div>
	  <div class="row">
	  	<div class="col-xs-6">
	  		<strong><?php echo $GLOBALS["lang"]["country"]; ?>:</strong>
	  		<p><?php echo $userdata["country"]; ?></p>
	  	</div>
	  	<div class="col-xs-6">
	  		<strong><?php echo $GLOBALS["lang"]["phone"]; ?>:</strong>
	  		<p><?php echo $userdata["phone"]; ?></p>
	  	</div>
	  </div>
	  <div class="row">
	  	<div class="col-md-12">
	  		<strong><?php echo $GLOBALS["lang"]["state_province"]; ?>:</strong>
	  		<p><?php echo $userdata["state"]; ?></p>
	  	</div>
	  </div>
	  <div class="row">
	  	<div class="col-md-12">
	  		<strong><?php echo $GLOBALS["lang"]["city"]; ?>:</strong>
	  		<p><?php echo $userdata["city"]; ?></p>
	  	</div>
	  </div>
	  <div class="row">
	  	<div class="col-xs-6">
	  		<strong><?php echo $GLOBALS["lang"]["lvlaccess"]; ?>:</strong>
	  		<?php foreach ($lvlaccesslist as $a) { ?>
	  			<?php if($a["id"]==$userdata["lvlaccess"]){ ?>
	  			<p><?php echo $a["name"]; ?></p>
	  			<?php } ?>
	  		<?php } ?>
	  	</div>
	  	<div class="col-xs-6">
	  		<strong><?php echo $GLOBALS["lang"]["status"]; ?>:</strong>
	  		<p><?php echo ($userdata["status"]==0)? $GLOBALS["lang"]["active"] : $GLOBALS["lang"]["disable"] ; ?></p>
	  	</div>
	  </div>
	  <div class="row">
	  	<div class="col-md-12 texthright">
	  		<a href="<?php echo url("index.php?a=editprofile"); ?>" class="btn btn-primary"><?php echo $GLOBALS["lang"]["edit"]; ?> <?php echo $GLOBALS["lang"]["profile"]; ?></a>
	  	</div>
	  </div>
  </div>
</div>





<?php 
include surl("app".DS."view".DS."layouts".DS."footer.php");

include surl("app".DS."view".DS."layouts".DS."script.php");
?>
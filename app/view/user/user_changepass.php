<?php 

include surl("app".DS."view".DS."layouts".DS."htmlheader.php");

include surl("app".DS."view".DS."layouts".DS."header.php");
?>

<div>
		<nav class="pd-lr">
			<h1 class="texthap"><i class="glyphicon glyphicon-dashboard"></i> <?php echo $GLOBALS["lang"]["administration"]; ?> > <small><i class="glyphicon glyphicon-user"></i> <?php echo $GLOBALS["lang"]["changemypass"] ?></small></h1>
		</nav>
</div>

<div class="panel panel-default m-l m-r">
  <!-- Default panel contents -->
  <div class="panel-heading"><span class="glyphicon glyphicon-plus"></span> <?php echo $GLOBALS["lang"]["adduser"]; ?></div>

  <div class="panel-body">
  	<form name="formchangemypass" id="formchangemypass" action="" method="post" onsubmit="return false;">
  		<div class="form-group">
  			<label><?php echo $GLOBALS["lang"]["currentpassword"]; ?></label>
	  		<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
	  			<input type="password" name="currentpassword" id="currentpassword" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_currentpassword"]; ?>" />
	  		</div>
  		</div>
		<div class="form-group">
			<label><?php echo $GLOBALS["lang"]["newpassword"]; ?></label>
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
				<input type="password" name="newpassword" id="newpassword" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["type_newpassword"]; ?>" />
			</div>
		</div>

		<div class="form-group">
			<label><?php echo $GLOBALS["lang"]["repassword"]; ?>:</label>
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
				<input type="password" name="repassword" id="repassword" class="form-control" placeholder="<?php echo $GLOBALS["lang"]["repassword"]; ?>" />
			</div>
		</div>

		<div class="form-group">
  			<button type="reset" class="btn btn-warning"><?php echo $GLOBALS["lang"]["reset"]; ?></button>
  			<button type="submit" id="store" name="store" class="btn btn-primary pull-right"><?php echo $GLOBALS["lang"]["changemypass"]; ?></button>
  		</div>
	</form>
   </div>
</div>


<?php 
include surl("app".DS."view".DS."layouts".DS."footer.php");

include surl("app".DS."view".DS."layouts".DS."script.php");
?>
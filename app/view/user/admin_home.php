<?php 

include surl("app".DS."view".DS."layouts".DS."htmlheader.php");

include surl("app".DS."view".DS."layouts".DS."header.php");
?>


<div>
		<nav class="pd-lr">
			<h1 class="texthap"><i class="glyphicon glyphicon-dashboard"></i> <?php echo $GLOBALS["lang"]["administration"]; ?> > <small><i class="glyphicon glyphicon-home"></i> <?php echo $GLOBALS["lang"]["index"]; ?></small></h1>
		</nav>
</div>
<div class="panel panel-default m-l m-r">
  <!-- Default panel contents -->
  <div class="panel-heading"><?php echo $GLOBALS["lang"]["index"]; ?></div>
  <div class="panel-body txthcenter">
  	<br/><br/>
    <h1><?php echo $GLOBALS["lang"]["wtgui"]; ?> de Microlar framework.</h1>
    <br/>
    <br/>
    <br/>
    <br/>
  </div>
</div>


<?php 
include surl("app".DS."view".DS."layouts".DS."footer.php");

include surl("app".DS."view".DS."layouts".DS."script.php");
?>
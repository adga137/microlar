<!DOCTYPE html>
<html>
<head>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.png">
	<link rel="stylesheet" type="text/css" href="<?php echo url("vendor/bootstrap-3.3.7-dist/css/bootstrap.min.css"); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo url("css/style.css"); ?>">
	<title><?php echo $GLOBALS["lang"]["passrecover"]; ?> Microlar Framework</title>
</head>
<body class="bg-body">
	<div class="container-fluid">
		<div class="row">
			<div class="box-login">
				<div class="box-login-header">
					<img src="<?php echo url("img/microlar.png"); ?>" class="img-logo">
					<p class="login-box-text"><?php echo $GLOBALS["lang"]["passrecover"]; ?></p>
				</div>
				<div class="box-body" >
					<p class="recover-box-msg"><?php echo $GLOBALS["lang"]["psrmsg"]; ?></p>
					<form class=""  name="loginform" id="loginform" action="<?php echo url("index.php?a=access"); ?>" method="post" >
						<div class="box-login-content">
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-addon" id="basic-addon1"><i>@</i></span>
									<input type="email" class="form-control" id="email" name="email" placeholder="<?php echo $GLOBALS["lang"]["type_email"]; ?>" />
								</div>
							</div>
							<div class="form-group mh-25">
								<button class="btn btn-primary pull-right"><?php echo $GLOBALS["lang"]["send_email"]; ?></button>
							</div>
						</div>
					</form>
					<a class="link-login" href="<?php echo url("index.php?a=login"); ?>"><?php echo $GLOBALS["lang"]["login"]; ?></a>
				</div>
			</div>
		</div>
	</div>
</body>
<script type="text/javascript" src="<?php echo url("vendor/jquery-2.2.4/jquery-2.2.4.min.js"); ?>"></script>
<script type="text/javascript" src="<?php echo url("vendor/bootstrap-3.3.7-dist/js/bootstrap.min.js"); ?>"></script>
</html>
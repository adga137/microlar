<?php

require_once surl("app".DS."controllers".DS."SessionController.php");

$pwd="";

//verify active session
if((new SessionController)->checksession()){
    //user routes
    if(isset($_GET["a"])){
        $pwd=$_GET["a"];
        switch($_GET["a"]){
            case "home":
                require_once surl("app".DS."controllers".DS."UserController.php");
                (new UserController)->home();
                break;

            case "users":
                require_once surl("app".DS."controllers".DS."UserController.php");
                if(isset($_GET["page"])){
                    if($_GET["page"]!=null){
                        if(isset($_GET["search"])){
                            if ($_GET["search"]!=null) {
                                (new UserController)->listusers( $_GET["page"], $_GET["search"]);
                            }
                        }else{
                            (new UserController)->listusers($_GET["search"]);
                        }
                    }

                }else if(isset($_GET["search"])){
                        if ($_GET["search"]!=null) {
                            (new UserController)->listusers($_GET["page"], $_GET["search"]);
                        }
                    
                }else{
                    (new UserController)->listusers();
                }
                break;

            case 'createuser':
                require_once surl("app".DS."controllers".DS."UserController.php");
                (new UserController)->create();
                break;

            case 'profile':
                require_once surl("app".DS."controllers".DS."UserController.php");
                (new UserController)->profile();
                break;

            case 'editprofile':
                require_once surl("app".DS."controllers".DS."UserController.php");
                (new UserController)->editprofile();
                break;

            case 'changemypass':
                require_once surl("app".DS."controllers".DS."UserController.php");
                (new UserController)->sessionchangepass();
                break;

            case "access":
                header("location: ".url("index.php?a=home"));
                break;

            case "help":
                require_once surl("app".DS."controllers".DS."UserController.php");
                (new UserController)->manual();
                break;

            // api actions
            case "api":
                require_once surl("app".DS."controllers".DS."ApiController.php");

                if(isset($_GET["enctype"])){
                    if($_GET["enctype"] == "multipart-formdata"){
                        $data = $_POST;
                        $files = $_FILES;
                        unset($data["params"]);
                        (new ApiController)->run($_POST['params'],$data,$files);
                    }
                }else{
                    if(!isset($_POST["data"])){
                        (new ApiController)->run($_POST['params']);
                    }else{
                        (new ApiController)->run($_POST['params'],$_POST["data"]);
                    }
                }

                break;
            
            case "logout":
                (new SessionController)->logout();
                break;
            
            default:
                require_once surl("app".DS."controllers".DS."UserController.php");
                (new UserController)->notfound();
                break;
        }
    }else{
        require_once surl("app".DS."controllers".DS."UserController.php");
        (new UserController)->home();
    }
}else{
    // public actions
    if(isset($_GET["a"])){
        $pwd=$_GET["a"];
        switch($_GET["a"]){
            case "login":
                include surl("app".DS."view".DS."login.php");
                break;
            case "passrecover":
                include surl("app".DS."view".DS."passrecover.php");
                break;
            case "access":
                require_once surl("app".DS."controllers".DS."UserController.php");
                (new UserController)->login(array(
                        'loginemail' => $_POST["user"],
                        'loginpassword' => $_POST["password"] 
                    ));
                break;
            case "sendrecoverpass":
                require_once surl("app".DS."controllers".DS."MailController.php");
                (new MailController)->sendrecoverpass($_POST["email"]);
                break;
            case "changepass":
                require_once surl("app".DS."controllers".DS."UserController.php");
                (new UserController)->changepass($_GET["rptoken"]);
                break;
            case "processchangepass":
                require_once surl("app".DS."controllers".DS."UserController.php");
                (new UserController)->processchangepass($_POST);
                break;

            //api return { "session" : false }
            case "api":
                echo json_encode(array( "session" => false ));
                break;


            default:
                include surl("app".DS."view".DS."login.php");
                break;
        }
    }else{
        include surl("app".DS."view".DS."index.php");
    }
}

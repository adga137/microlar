<?php
require_once surl("app".DS."kernel".DS."framework".DS."microlar".DS."Model.php");

class Country extends Model{

	//contructor de la clase
	public function __contruct(){}


	public function countrieslist(){
		return $this->db->get("countries");
	}

	public function getcountryname($countrycode){
		$this->db->where("country_code", $countrycode);
		$country = $this->db->getOne("countries");
		return $country["country_name"];
	}	

	//destructor de la clase
	public function __destruct(){}
}
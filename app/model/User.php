<?php

require_once surl("app".DS."kernel".DS."framework".DS."microlar".DS."Model.php");

class User extends Model{

	public function __contruct(){}

	// adding new user
	public function store($data){
		$data = Array (
				"user"					=> $data["user"],
				"email"					=> $data["email"],
				"firstname"				=> $data["firstname"], 
				"lastname"				=> $data["lastname"],
				"country"				=> $data["country"],
				"phone"					=> $data["phone"],

				"state"					=> $data["state"],
				"city"					=> $data["city"],
				"address"				=> $data["address"],
				"lvlaccess"				=> $data["lvlaccess"],
				"status"				=> $data["status"],
				"isDelete"				=> 0,
				"created"				=> date("Y-m-d H:i:s"),

				"password"		=> sha1(crypt($data["password"],$GLOBALS["key"]))
			);

		$rs = $this->db->insert('users', $data);

		if($data["status"]==3 && isset($data["package"])){
			$this->db->join("assignments a", "a.user_id=u.id", "LEFT");
			$this->db->where("u.email", $data["email"]);
			$user_id = $this->db->get ("users u", null, "u.id");

			if($data["package"]!=null || $data["package"]!=""){
				$data = Array (
					"userid"					=> $user_id[0]["id"],
					"package"					=> $data["package"]
				);
				$rs = Assignment::assignpackage($data);
			}
		}

		if($rs){
			return true;
		}else{
			return false;
		}
	}

	// logic delete user
	public function delete($id){
		$data = Array (
						'isDelete' => 1
					);
		$this->db->where('id', $id);
		$rs=$this->db->update('users', $data);
		if($rs){
			return true;
		}else{
			return false;
		}
	}

	public function totalusers(){
		$this->db->where("isDeleted",0);
		return $this->db->getValue ("users", "count(*)");
	}

	// 
	public function userlist($page, $search ){

		$this->db->where('isDelete', 0);
	
		
		$this->db->pageLimit = 25;

		$users = $this->db->arraybuilder()->paginate("users", $page);

		$totalpages = $this->db->totalPages;


		return array('users' => $users, 'totalpages'  => $totalpages, 'actualpage' => $page );

		//return false;
	}

	public function namelvlaccess($lvlaccess){
		$this->db->where('id', $lvlaccess);
		$rs = $this->db->get("lvlaccess");
		return $rs[0]["name"];
	}

	public function lvlaccesslist(){
		return $this->db->get("lvlaccess");;
	}

	public function childslvlaccess($lvlaccess){
		$this->db->where('id', $lvlaccess, '>');
		return $this->db->get("lvlaccess");
	}

	public function userfind($search,$status){
		//$params = Array($status, $search, $search, $search, $search);
		return $this->db->rawQuery("SELECT * FROM users WHERE status=".$status." and (firstname LIKE '%".$search."%' or lastname LIKE '%".$search."%' or email LIKE '%".$search."%' or eps_medical_insurance LIKE '%".$search."%');");
	}

	// this return user data
	public function getuserdata($id){
		$this->db->where('id', $id);
		return $this->db->getOne("users");;
	}


	public function update($data){
		$this->db->where('id', $data["userid"]);
		if($data["password"]!="" && $data["repassword"]!=""){
			$data = Array (
						'email' => $data["email"],
						'password' => sha1(crypt($data["password"],$GLOBALS["key"])),
						'firstname' => $data["firstname"],
						'lastname' => $data["lastname"],
						'age' => $data["age"],
						'phone' => $data["phone"],
						'birthdate' => $data["birthdate"],
						'facebook' => $data["facebook"],
						'ephone' => $data["ephone"],
						'bloodtype' => $data["bloodtype"],
						'contraindicationstm' => $data["contraindicationstm"],
						'observations' => $data["observations"],
						'eps_medical_insurance' => $data["eps_medical_insurance"],
						'address' => $data["address"]
					);
		}else{
			$data = Array (
						'email' => $data["email"],
						'firstname' => $data["firstname"],
						'lastname' => $data["lastname"],
						'age' => $data["age"],
						'phone' => $data["phone"],
						'birthdate' => $data["birthdate"],
						'facebook' => $data["facebook"],
						'ephone' => $data["ephone"],
						'bloodtype' => $data["bloodtype"],
						'contraindicationstm' => $data["contraindicationstm"],
						'observations' => $data["observations"],
						'eps_medical_insurance' => $data["eps_medical_insurance"],
						'address' => $data["address"]
					);
		}
		return $this->db->update ('users', $data);
	}

	// reset password to '123456'
	public function resetpassword($id){
		$this->db->where('id', $id);
		$data = Array (
					'password' => sha1(crypt('123456',$GLOBALS["key"]))
				);
		return $this->db->update ('users', $data);
	}

	// reset user password to 123456
	public function updatemypass($id, $data){
		$this->db->where('id', $id);
		$data = Array (
					'password' => sha1(crypt( $data["newpassword"] ,$GLOBALS["key"])),
					'updated'  => date("Y-m-d H:i:s")
				);
		return $this->db->update('users', $data);
	}

	public function checkuser($login){

		$this->db->where('email', $login["loginemail"]);
		$this->db->orWhere('user', $login["loginemail"]);
		$this->db->where('password', sha1(crypt($login["loginpassword"],$GLOBALS["key"])));


		//$sql="SELECT * FROM users WHERE (user=? or email=?) and password=? ;";

		//$result = $this->db->rawQuery ($sql, array($login["loginemail"],$login["loginemail"],sha1(crypt($login["loginpassword"],$GLOBALS["key"]))));



		$result = $this->db->get("users");

		if(isset($result[0]["id"])){
			return $result[0]["id"];
		}else{
			return false;
		}
	}

	// create session token
	public function createsessiontoken($iduser){


		$this->db->where('user_id', $iduser);
		$this->db->where('status', 'a');
		$this->db->orderBy("created","desc");
		$rs = $this->db->get("tokens");

		foreach ($rs as $token) {
			$data = Array (
						'status' => 'd'
					);
			$this->db->where ('id', $token["id"]);
			$this->db->update ('tokens', $data);
		}


		$h24 = strtotime ( '+24 hours' , strtotime ( date('Y-m-d H:i:s') ) ) ;
		$h24 = date ( 'Y-m-d  H:i:s' , $h24 );

		do{

			$data = Array (
					"user_id" => $iduser,
					"token"   => sha1($this->generateRandomString()),
	            	"created" => date("Y-m-d H:i:s"),
	            	"expire"  => $h24,
	            	"status"  => 'a'
				);
		
			$rs=$this->db->insert('tokens', $data);
			
		}while(!$rs);

		$this->db->where('user_id', $iduser);
		$this->db->where('status', 'a');
		$this->db->orderBy("created","desc");
		$result = $this->db->get("tokens",1);

		return $result;
	}

	// verify session token
	public function checktoken($token){
		//falta verificar el tiempo de vida del token
		$this->db->where('token', $token)
					  ->where('status','a')
					  ->orderBy("created","desc");

		$result = $this->db->get("tokens",1);
		if(isset($result[0]["token"])){
			if($result[0]["token"]!="" or $result[0]["token"]!=null){
				if(strtotime ( date('Y-m-d H:i:s') ) <= strtotime($result[0]["expire"]) ){
					return true;
				}else{
					$data = Array (
						'status' => 'd'
					);
					$this->db->where ('token', $token);
					$this->db->update ('tokens', $data);
					return false;
				}
				
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	// destroy session token
	public function deltoken($token){
		$data = Array (
						'status' => 'd'
					);
		$this->db->where ('token', $token);
		$this->db->update ('tokens', $data);
	}


	// return session userdata
	public function sessionuserdata($token){
		
		$this->db->join("tokens t", "t.user_id=u.id", "LEFT");
		$this->db->where("t.token", $token);
		$result = $this->db->get ("users u", null, " u.*");

		return $result[0];
	}

	// return lvlaccess from session user
	public function getlvlaccess($token){
		$this->db->join("tokens t", "t.user_id=u.id", "LEFT");
		$this->db->where("t.token", $token);
		$statususer = $this->db->get ("users u", null, "u.lvlaccess");
		return $statususer[0]["lvlaccess"];
	}

	// return fullname from session user
	public function getfullname($token){
		$this->db->join("tokens t", "t.user_id=u.id", "LEFT");
		$this->db->where("t.token", $token);
		$statususer = $this->db->get ("users u", null, "u.firstname , u.lastname");
		return $statususer[0]["firstname"]." ".$statususer[0]["lastname"];
	}

	// rand
	public function generateRandomString($length = 10) {
	    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
	    $charactersLength = strlen($characters);
	    $randomString = '';
	    for ($i = 0; $i < $length; $i++){
	    	$randomString .= $characters[rand(0, $charactersLength - 1)];
	    }
	    return $randomString;
	} 

	// sending recover password email
	public function getuserdatabyemail($email){
		$this->db->where("email", $email);
		$user = $this->db->get("users");

		if(isset($user[0])){
			$user=$user[0];
			if($user["email"]==$email){
				return $user;
			}else{
				return false;
			}
		}else{
			return false;
		}
	}

	// creating recover pass token
	public function rptoken($iduser){

		$this->db->where('user_id', $iduser);
		$this->db->where('status', 'a');
		$this->db->orderBy("created","desc");
		$rs = $this->db->get("recoverpasstokens");

		foreach ($rs as $token) {
			$data = Array (
						'status' => 'd'
					);
			$this->db->where ('id', $token["id"]);
			$this->db->update ('recoverpasstokens', $data);
		}

		$h24 = strtotime ( '+24 hours' , strtotime ( date('Y-m-d H:i:s') ) ) ;
		$h24 = date ( 'Y-m-d  H:i:s' , $h24 );
		
		$rptoken = "";
		do{
			$rptoken = sha1($this->generateRandomString());
			$data = Array (
					"user_id" => $iduser,
					"token"   => $rptoken,
	            	"created" => date("Y-m-d H:i:s"),
	            	"expire"  => $h24,
	            	"status"  => 'a'
				);
		
			$rs=$this->db->insert('recoverpasstokens', $data);
			
		}while(!$rs);

		return $rptoken;
	}

	public function verifyrptoken($rptoken){
		
		//falta verificar el tiempo de vida del token
		$this->db->where('token', $rptoken)
					  ->where('status','a')
					  ->orderBy("created","desc");

		$result = $this->db->get("recoverpasstokens",1);
		if(isset($result[0]["token"])){
			if($result[0]["token"]!="" or $result[0]["token"]!=null){
				if(strtotime ( date('Y-m-d H:i:s') ) <= strtotime($result[0]["expire"]) ){
					return true;
				}else{
					$data = Array (
						'status' => 'd'
					);
					$this->db->where ('token', $rptoken);
					$this->db->update ('recoverpasstokens', $data);
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}

		return false;
	}

	public function changepass($data){
		$this->db->where("token", $data["token"]);
		$userid = $this->db->get("recoverpasstokens");
		$userid=$userid[0];

		$sqldata = Array (
						'password' => sha1(crypt($data["password"],$GLOBALS["key"]))
					);

		$this->db->where("user_id", $userid);
		$result = $this->db->update("users",$sqldata);

		if($result){
			return true;
		}else{
			return false;
		}
	}


	// destructor
	public function __destruct(){}

}

?>
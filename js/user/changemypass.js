
//ajax change pass session user
function changemypass(){
	if(formvalidator()){
		swal({
		  title: lang["warninguschangepasstitle"],
		  text: lang["warninguschangepasstext"],
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: lang["yes"],
		  cancelButtonText: lang["no"],
		  closeOnConfirm: false,
		  closeOnCancel: true
		},
		function(isConfirm){
		  if (isConfirm) {


		  	$.ajax({
		        type: 'POST',
		        url: urlbs+'index.php?a=api',
		        data: {
		        	'params'	: "updatemypass",
		        	'data' 		: {
	        			'currentpassword' 		: $("body").find("#formchangemypass").find("#currentpassword").val(),
	        			'newpassword'	 		: $("body").find("#formchangemypass").find("#newpassword").val(),
	        			'repassword' 			: $("body").find("#formchangemypass").find("#repassword").val(),

	        		}
		        },
		        dataType: 'json',
		        success: function(data){


		        	if((typeof data["status"])!='undefined'){

			        	switch(parseInt(data["status"])){
			        		case 0:
			        			swal(lang["chpass_successtitle"], lang["chpass_successtext"], "success");
			        			break;
			        		case 1:
			        			swal(lang["chpass_errortitle2"], lang["chpass_errortext2"], "error");
			        			break;


			        		default:
			        			swal(lang["chpass_errortitle"], lang["chpass_errortext"], "error");
			        			break;
			        	}
		        	}else{
		        		swal(lang["conerror"], lang["conerrortxt"], "error");
		        	}
		            
		        },
		        error: function(xhr, status) {
		            swal(lang["conerror"], lang["conerrortxt"], "error");
		        }
		    });

		  }
		});
	}else{
		swal(lang["emptytitle"], lang["emptytext"], "error");
	}
}

// validator changepassform
function formvalidator(){

	var validatestatus = true;

	$("body").find("#formchangemypass").find('input').each(function(i, field) {
			if( field.value==null || field.value=="")
				validatestatus = false;
		});

	if(
		$("body").find("#formchangemypass").find("#newpassword").val() !=
		$("body").find("#formchangemypass").find("#repassword").val()
		){
		validatestatus = false;
	}

	return validatestatus;
}
function adduser(urlbs){

	swal({
	  title: lang["warningadduser"],
	  text: lang["warningaddusertext"],
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: lang["yes"],
	  cancelButtonText: lang["no"],
	  closeOnConfirm: false,
	  closeOnCancel: true
	},
	function(isConfirm){
	  if (isConfirm) {

  		// load formdata object
		/*form = $("body").find("#formadduser");
		var total = 0;
		var elements = [];
		$("body").find("#formadduser").find('input, textarea, select').each(function(i, field) {
			elements.push(field);
		}); 

		for(var key in elements) {
		    if(elements.hasOwnProperty(key)){
		        total++;
		    }
		}
		
		formData = new FormData();
			
		for (var i = 0; i < total; i++){
		    
		    if( typeof elements[i].type !== "undefined" ){
			    if (elements[i].type == "file"){
			        var file = elements[i].files;
			        formData.append(elements[i].name, file[0]);
			    }else{
			        formData.append(elements[i].name, elements[i].value);
			    }
			}else{
				formData.append(elements[i].name, elements[i].value);
			}
			
		}*/
		// end load formdata object

		var fs = $("body").find("#formadduser").serializeArray();
		var fa = {};

		for (var i = 0; i < fs.length; i++) {
			fa[fs[i]["name"]] = fs[i]["value"];
		}


	  	$.ajax({
	        type: 'POST',
	        url: window.urlbs+'index.php?a=api',
	        data: {
	        	"params"	: "userstore",
	        	"data"		:  fa 
	        },
	        dataType: 'json',
	        success: function(data){
	                    
	            if(data["res"]){
	            	swal(lang["successadduser"], lang["successaddusertext"] , "success");
	            	$("body").find("#formadduser").trigger("reset");
	            }else{
	            	swal(lang["internalerrortitle"], lang["internalerrortext"], "error");
	            }
	            
	        },
	        error: function(xhr, status) {
	            swal(lang["conerror"], lang["conerrortxt"], "error");
	        }
	    });

	  }else{
	    swal(lang["cancelledadduser"], lang["cancelledaddusertext"] , "error");
	  }
	});
}
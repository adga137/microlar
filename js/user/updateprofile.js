
// send request ajax update profile
function updateprofile(){
	
	if(formvalidator()){
		swal({  
		  title: lang["warningupdateprofile"],
		  text: lang["warningupdateprofiletext"],
		  type: "warning",
		  showCancelButton: true,
		  confirmButtonColor: "#DD6B55",
		  confirmButtonText: lang["yes"],
		  cancelButtonText: lang["no"],
		  closeOnConfirm: false,
		  closeOnCancel: true
		},
		function(isConfirm){
		  if (isConfirm) {

	  		// load formdata object
			form = $("body").find("#formprofile");
			var total = 0;
			var elements = [];
			$("body").find('#formprofile').find('input, textarea, select').each(function(i, field) {
				elements.push(field);
			}); 



			for(var key in elements) {
			    if(elements.hasOwnProperty(key)){
			        total++;
			    }
			}
			
			formData = new FormData();
				
			for (var i = 0; i < total; i++){
			    
			    if( typeof elements[i].type !== "undefined" ){
				    if (elements[i].type == "file"){
				        var file = elements[i].files;
				        formData.append(elements[i].name, file[0]);
				    }else{
				        formData.append(elements[i].name, elements[i].value);
				    }
				}else{
					formData.append(elements[i].name, elements[i].value);
				}
				
			}
			// end load formdata object


			formData.append("params", "updateprofile");


		  	$.ajax({
		        type: 'POST',
		        url: urlbs+'index.php?a=api&enctype=multipart-formdata',
		        data: formData,
		        enctype: 'multipart/form-data',
		        dataType: 'json',
				contentType: false, 
				processData: false,
				cache: false,
		    }).done(function (res){

		    	if(res["status"]==true){
					swal(lang["successupdatedprofile"], lang["updatedprofiletext"] , "success");
		    	}else{
		    		swal(lang["updateprofile_errortitle"], lang["conerrortxt"], "error");
		    	}

		    }).error(function (res){
		    	swal(lang["conerror"], lang["updateprofile_errortext"], "error");
		    });

		  }
		});
	}else{
		swal(lang["emptytitle"], lang["emptytext"], "error");
	}
}

// preview upload img
function imageToBase64(imgp,inpf){
  var preview = imgp; //document.querySelector('#vuserimg');
  var file    = inpf; //document.querySelector('input[type=file]').files[0];
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file);
  }
}

// validator formprofile
function formvalidator(){

	var validatestatus = true;

	$("body").find("#formprofile").find('input').each(function(i, field) {
			if( field.value==null || field.value=="")
				validatestatus = false;
		});

	return validatestatus;
}
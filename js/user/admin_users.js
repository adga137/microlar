
// searching user
function searchuser(){

}


//ajax delete user
function deleteuser(id){
	swal({
	  title: lang["warninguserdelete"],
	  text: lang["warninguserdeletetext"],
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#DD6B55",
	  confirmButtonText: lang["yes"],
	  cancelButtonText: lang["no"],
	  closeOnConfirm: false,
	  closeOnCancel: true
	},
	function(isConfirm){
	  if (isConfirm) {
	    
	  	$.ajax({
	        type: 'POST',
	        url: urlbs+'index.php?a=api',
	        data: {
	        	'params'	: "deleteuser",
	        	'data' 		: {

	        		'id'	: id
	        	
	        	}
	        },
	        dataType: 'json',
	        success: function(data){

	        	switch(data["res"]){
	        		case 0:
	        			swal({
	        				title: lang["successdeleteuser"],
  							text: lang["successdeleteusertext"],
  							timer: 2000,
  							type: "success",
  							showConfirmButton: false
	        			},
	        			function(){
							setTimeout(function(){
							  window.location.reload();
							}, 2000);
						});

	        			break;

	        		case 1:
	        			swal(lang["dunotpermitedtitle"], lang["dunotpermitedtext"], "error");
	        			break;

	        		default:
	        			swal(lang["internalerrortitle"], lang["internalerrortext"], "error");
	        			break;
	        	}
	            
	        },
	        error: function(xhr, status) {
	            swal(lang["conerror"], lang["conerrortxt"], "error");
	        }
	    });
	  }
	});
}

function edituser(id){

}

function viewuser(id){
	$.ajax({
        type: 'POST',
        url: urlbs+'index.php?a=api',
        data: {
        	'params'	: "usergetdata",
        	'data' 		: id
        },
        dataType: 'json',
        success: function(data){

            $("#viewModal").find("#user").text(data["user"]);
            $("#viewModal").find("#email").text(data["email"]);
            $("#viewModal").find("#firstname").text(data["firstname"]);
            $("#viewModal").find("#lastname").text(data["lastname"]);



            $("#viewModal").find("#country").text( data["country_name"]);
            $("#viewModal").find("#phone").text(data["phone"]);
            $("#viewModal").find("#state").text(data["state"]);
            $("#viewModal").find("#city").text(data["city"]);
            $("#viewModal").find("#address").text(data["address"]);
            $("#viewModal").find("#status").text( (parseInt(data["status"])==0)? lang["active"] : lang["disable"] );
            $("#viewModal").find("#access").text( lang["lvlaccessname"][data["lvlaccess"]] );

            
        },
        error: function(xhr, status) {
            swal(lang["conerror"], lang["conerrortxt"], "error");
        }
    });

}
/*
function getcountryname(countrycode){
	var cname = $.get();
	return cname;
}
*/